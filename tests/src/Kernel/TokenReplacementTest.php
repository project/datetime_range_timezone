<?php

namespace Drupal\Tests\datetime_range_timezone\Kernel;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Tests\token\Functional\TokenTestTrait;

/**
 * Tests token replacements.
 *
 * @group datetime_range_timezone
 */
class TokenReplacementTest extends KernelTestBase {

  use DateRangeTimezoneHelperTrait;
  use TokenTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'system',
    'field',
    'filter',
    'text',
    'token',
    'entity_test',
    'datetime',
    'datetime_range',
    'datetime_range_timezone',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig(['system']);
    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('user');

    EntityViewDisplay::create([
      'targetEntityType' => 'entity_test',
      'bundle' => 'entity_test',
      'mode' => 'default',
    ])->save();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'date',
      'entity_type' => 'entity_test',
      'type' => 'daterange_timezone',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ]);
    $field_storage->save();

    $field_config = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'entity_test',
    ]);
    $field_config->save();
  }

  /**
   * Tests support for a daterange_timezone fields.
   */
  public function testDateRangeTimezoneFieldTokens() {
    // Create an entity with a value in its fields and test its tokens.
    $start_date = new DrupalDateTime('2022-03-25 10:30:00', 'UTC');
    $end_date = new DrupalDateTime('2022-03-28 10:30:00', 'UTC');

    // The first date will be in 'EST' timezone.
    $entity = $this->createTestEntity($start_date, $end_date, 'EST');

    // Add another date value to test multiple field value deltas.
    $start_date = new DrupalDateTime('2014-08-22 10:30:00', 'UTC');
    $end_date = new DrupalDateTime('2017-12-20 10:30:00', 'UTC');
    $entity->get('date')->appendItem([
      'value' => $start_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => $end_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'timezone' => 'UTC',
    ]);
    $entity->save();

    $this->assertTokens('entity_test', ['entity_test' => $entity], [
      'date:start_date:html_date' => '2022-03-25',
      // The expected time value will be different because token will convert
      // the date into the given 'EST' (-5 hours) timezone for the replacement.
      'date:start_date:custom:Y-m-d H:i:s' => '2022-03-25 05:30:00',
      'date:end_date:custom:Y-m-d H:i:s' => '2022-03-28 05:30:00',
      'date:start_date' => $entity->get('date')->start_date->getTimestamp(),
      'date:timezone' => 'EST',
      'date:0:start_date:html_month' => '2022-03',
      'date:0:start_date:custom:Y' => '2022',
      'date:0:end_date:custom:Y' => '2022',
      'date:0:start_date' => $entity->get('date')->start_date->getTimestamp(),
      'date:0:timezone' => 'EST',
      'date:1:start_date:html_month' => '2014-08',
      'date:1:start_date:custom:Y-m-d H:i:s' => '2014-08-22 10:30:00',
      'date:1:end_date:custom:Y-m-d H:i:s' => '2017-12-20 10:30:00',
      'date:1:end_date' => $entity->get('date')->get(1)->end_date->getTimestamp(),
      'date:1:timezone' => 'UTC',
    ]);
  }

}
