<?php

namespace Drupal\Tests\datetime_range_timezone\Functional;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;

/**
 * Test the datetime widget.
 *
 * @group datetime_range_timezone
 */
class DateRangeTimezoneWidgetTest extends DateRangeTimezoneTestBase {

  /**
   * Ensure that default values are translated into the selected timezone.
   */
  public function testWidgetDefaultValuesAreTranslated() {
    $assert = $this->assertSession();

    // Create an entity with a date that we'll enter in America/New York time.
    $start_date = new DrupalDateTime('2017-03-25 10:30:00', 'America/New_York');
    $end_date = new DrupalDateTime('2017-03-28 10:30:00', 'America/New_York');

    // Submit a valid date and ensure it is accepted.
    $date_format = DateFormat::load('html_date')->getPattern();
    $time_format = DateFormat::load('html_time')->getPattern();

    $this->drupalGet('entity_test/add');

    // Test that the timezone select shows the default system timezone when the
    // form is loaded.
    $system_timezone = date_default_timezone_get();
    $this->assertTrue($assert->optionExists('Timezone', $system_timezone)->isSelected());

    $edit = [
      'date[0][value][date]' => $start_date->format($date_format),
      'date[0][value][time]' => $start_date->format($time_format),
      'date[0][end_value][date]' => $end_date->format($date_format),
      'date[0][end_value][time]' => $end_date->format($time_format),
      'date[0][timezone]' => 'America/New_York',
    ];
    $this->submitForm($edit, 'Save');

    $assert->fieldValueEquals('date[0][value][date]', $start_date->format($date_format));
    $assert->fieldValueEquals('date[0][value][time]', $start_date->format($time_format));
    $assert->fieldValueEquals('date[0][end_value][date]', $end_date->format($date_format));
    $assert->fieldValueEquals('date[0][end_value][time]', $end_date->format($time_format));
    $this->assertEquals('America/New_York', $this->assertSession()->selectExists('Timezone')->getValue());

    // Assert the stored values of the entity.
    $entity = EntityTest::loadMultiple();
    $entity = reset($entity);
    // Convert the date back to storage timezone to assert the form value had
    // successfully massaged back to storage timezone.
    $start_date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $this->assertEquals($start_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT), $entity->get('date')->value);
    $end_date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    $this->assertEquals($end_date->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT), $entity->get('date')->end_value);

    // Load the field config and set default value to test the default timezone
    // is set to the system timezone.
    $field_config = FieldConfig::load('entity_test.entity_test.date');
    $field_config->setDefaultValue([
      'default_date_type' => 'now',
      'default_date' => 'now',
      'default_end_date_type' => 'now',
      'default_end_date' => 'now',
    ])->save();

    $this->drupalGet('entity_test/add');
    $this->assertTrue($assert->optionExists('Timezone', $system_timezone)->isSelected());

    // Check the previous entity form is not changed and the value is selected.
    $this->drupalGet($entity->toUrl('edit-form'));
    $this->assertEquals('America/New_York', $this->assertSession()->selectExists('Timezone')->getValue());
  }

}
