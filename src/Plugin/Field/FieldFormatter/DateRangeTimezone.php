<?php

namespace Drupal\datetime_range_timezone\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\TimeZoneFormHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Default' formatter for 'daterange_timezone'.
 *
 * @FieldFormatter(
 *   id = "daterange_timezone",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "daterange_timezone"
 *   }
 * )
 */
class DateRangeTimezone extends DateRangeTimezoneFormatterBase {

  use DateRangeTimezoneTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $build = [];
    foreach ($items as $delta => $item) {
      $end_date = '';
      $same_dates = $item->start_date->getTimestamp() === $item->end_date->getTimestamp();
      if (!empty($item->end_date) && !$same_dates) {
        // We show the end date if it is different from the start date.
        $end_date = $this->formatDate($item->end_date, $item->timezone);
      }

      $build[$delta] = [
        '#theme' => 'datetime_range_timezone',
        '#start_date' => $item->start_date ? $this->formatDate($item->start_date, $item->timezone) : '',
        '#iso_start_date' => $item->start_date ? $item->start_date->format('Y-m-d\TH:i:s', ['timezone' => $item->timezone]) . 'Z' : '',
        '#end_date' => $end_date,
        '#iso_end_date' => $end_date ? $item->end_date->format('Y-m-d\TH:i:s', ['timezone' => $item->timezone]) . 'Z' : '',
        '#separator' => $end_date ? $this->getSetting('separator') : '',
        '#timezone' => $this->getSetting('display_timezone') && $item->timezone ? TimeZoneFormHelper::getOptionsList()[$item->timezone] : NULL,
      ];
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'separator' => '-',
      'format_type' => 'medium',
      'display_timezone' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date separator'),
      '#description' => $this->t('The string to separate the start and end dates'),
      '#default_value' => $this->getSetting('separator'),
    ];

    $form['format_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Date format'),
      '#description' => $this->t("Choose a format for displaying the date. Be sure to set a format appropriate for the field, i.e. omitting time for a field that only has a date."),
      '#options' => $this->getDateFormatOptions(),
      '#default_value' => $this->getSetting('format_type'),
    ];

    $form['display_timezone'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Timezone'),
      '#description' => $this->t('Should we display the timezone after the formatted date?'),
      '#default_value' => $this->getSetting('display_timezone'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($separator = $this->getSetting('separator')) {
      $summary[] = $this->t('Separator: %separator', ['%separator' => $separator]);
    }

    $date = new DrupalDateTime();
    $summary[] = $this->t('Format: @display', ['@display' => $this->formatDate($date)]);

    $summary[] = $this->t('@action the timezone', [
      '@action' => $this->getSetting('display_timezone') ? 'Showing' : 'Hiding',
    ]);

    return $summary;
  }

}
