<?php

namespace Drupal\datetime_range_timezone\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\TimeZoneFormHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the single date plugin.
 *
 * @FieldFormatter(
 *   id = "daterange_timezone_single_date",
 *   label = @Translation("Single Date"),
 *   field_types = {
 *     "daterange_timezone"
 *   }
 * )
 */
class DateRangeTimezoneSingleDate extends DateRangeTimezoneFormatterBase {

  use DateRangeTimezoneTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $build = [];
    foreach ($items as $delta => $item) {
      $date_field = $this->getSetting('date_field');
      $date = $item->{$date_field} ? $this->formatDate($item->{$date_field}, $item->timezone) : '';
      $timezone = $this->getSetting('display_timezone') && $item->timezone ? ' ' . TimeZoneFormHelper::getOptionsList()[$item->timezone] : NULL;
      $build[$delta] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => sprintf('%s%s', $date, $timezone),
      ];
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'format_type' => 'medium',
      'date_field' => 'start_date',
      'display_timezone' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['date_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Date field'),
      '#description' => $this->t('Choose the date to display, either the start or end date.'),
      '#options' => [
        'start_date' => $this->t('Start Date'),
        'end_date' => $this->t('End Date'),
      ],
      '#default_value' => $this->getSetting('date_field'),
    ];

    $form['format_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Date format'),
      '#description' => $this->t("Choose a format for displaying the date. Be sure to set a format appropriate for the field, i.e. omitting time for a field that only has a date."),
      '#options' => $this->getDateFormatOptions(),
      '#default_value' => $this->getSetting('format_type'),
    ];

    $form['display_timezone'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Timezone'),
      '#description' => $this->t('Should we display the timezone after the formatted date?'),
      '#default_value' => $this->getSetting('display_timezone'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $date = new DrupalDateTime();
    $summary[] = $this->t('Format: @display', ['@display' => $this->formatDate($date)]);

    $summary[] = $this->t('@action the timezone', [
      '@action' => $this->getSetting('display_timezone') ? 'Showing' : 'Hiding',
    ]);

    return $summary;
  }

}
