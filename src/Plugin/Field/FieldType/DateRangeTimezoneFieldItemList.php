<?php

namespace Drupal\datetime_range_timezone\Plugin\Field\FieldType;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeFieldItemList;

/**
 * Represents a configurable entity daterange_timezone field.
 */
class DateRangeTimezoneFieldItemList extends DateRangeFieldItemList {

  /**
   * {@inheritdoc}
   */
  public static function processDefaultValue($default_value, FieldableEntityInterface $entity, FieldDefinitionInterface $definition) {
    // Explicitly call the parent class so that we can get the default values.
    $default_value = parent::processDefaultValue($default_value, $entity, $definition);

    if (!empty($default_value[0]['start_date'])) {
      $default_value[0]['timezone'] = date_default_timezone_get();
    }

    return $default_value;
  }

}
